use gloo::storage::{LocalStorage, Storage};
use serde::{Serialize, Deserialize};
use web_sys::HtmlInputElement;
use yew::{html, Component, Context, Html, Event, TargetCast, virtual_dom, Properties};
use crate::state::ev_list::{EvList, VehicleRangeInformation};

// =============================== Struct ==
#[derive(Debug, Serialize, Deserialize)]
pub struct Range {
    battery_capacity: f64,
    vehicle_efficiency: f64,
    reserved_range: f64,
    max_charge: f64,
    charging_rate: f64,
    speed: f64,
    pre_built_list: Vec<VehicleRangeInformation>,
}

// =============================== Msg ==
pub enum Msg {
    UpdateKwh(String),
    UpdateMpkwh(String),
    UpdateReserveRange(String),
    UpdateChargeStop(String),
    UpdateChargingRate(String),
    UpdateSpeed(String),
    Reset,
}

// =============================== Props ==
#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    #[prop_or(default_key())]
    pub storage_key: &'static str,
}

fn default_key() -> &'static str {
    "ev_range_key"
}

// =============================== Component ==
impl Component for Range {
    type Message = Msg;
    type Properties = Props;

    fn create(ctx: &Context<Self>) -> Self {
        let stored_data = 
            LocalStorage::get(&ctx.props().storage_key).unwrap_or_else(|_| {
                Range {
                    battery_capacity: 0.0,
                    vehicle_efficiency: 0.0,
                    reserved_range: 0.0,
                    max_charge: 0.0,
                    charging_rate: 0.0,
                    speed: 0.0,
                    pre_built_list: EvList::from(),
                }
            });
        Range { 
            battery_capacity: stored_data.battery_capacity,
            vehicle_efficiency: stored_data.vehicle_efficiency,
            reserved_range: stored_data.reserved_range,
            max_charge: stored_data.max_charge,
            charging_rate: stored_data.charging_rate,
            speed: stored_data.speed,
            pre_built_list: EvList::from(),
        }
    }

    // the return type of this determines if you need to re-render
    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::UpdateKwh(input) => {
                let input = input.parse::<f64>().unwrap_or(0_f64);
                self.battery_capacity = input;
            }

            Msg::UpdateMpkwh(input) => {
                let input = input.parse::<f64>().unwrap_or(0_f64);
                self.vehicle_efficiency = input;
            }

            Msg::UpdateReserveRange(input) => {
                let input = input.parse::<f64>().unwrap_or(0_f64);
                self.reserved_range = input;
            }

            Msg::UpdateChargeStop(input) => {
                let input = input.parse::<f64>().unwrap_or(0_f64);
                self.max_charge = input;
            }

            Msg::UpdateChargingRate(input) => {
                let input = input.parse::<f64>().unwrap_or(0_f64);
                self.charging_rate = input;
            }

            Msg::UpdateSpeed(input) => {
                let input = input.parse::<f64>().unwrap_or(0_f64);
                self.speed = input;
            }

            Msg::Reset => {
                self.battery_capacity = 0.0;
                self.vehicle_efficiency = 0.0;
                self.reserved_range = 0.0;
                self.max_charge = 0.0;
                self.charging_rate = 0.0;
                self.speed = 0.0;
            }
        }

        LocalStorage::set(&ctx.props().storage_key, &self)
            .expect("Failed tostore data in LocalStorage");
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let update_kwh = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateKwh(value))
        });

        let update_mpkwh = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateMpkwh(value))
        });

        let update_reserve_range = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateReserveRange(value))
        });

        let update_charge_stop = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateChargeStop(value))
        });

        let update_charging_rate = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateChargingRate(value))
        });

        let update_speed = ctx.link().batch_callback(|event: Event| {
            let input: HtmlInputElement = event.target_unchecked_into();
            let value = input.value();
            Some(Msg::UpdateSpeed(value))
        });

        let reset = ctx.link().callback(|_| Msg::Reset);

        let stored_data =
            LocalStorage::get(&ctx.props().storage_key).unwrap_or_else(|_|{
                Range {
                    battery_capacity: 0.0,
                    vehicle_efficiency: 0.0,
                    reserved_range: 0.0,
                    max_charge: 0.0,
                    charging_rate: 0.0,
                    speed: 0.0,
                    pre_built_list: EvList::from(),
                }
            });

        html! {
            <>
            <div class="row">
                <div class="input_column_halfs">
                    { self.input_block("Battery size (KWH)".to_string(), stored_data.battery_capacity.to_string(), update_kwh) }
                </div>
                <div class="input_column_halfs">
                    { self.input_block("Charge Stop (Percentage)".to_string(), stored_data.max_charge.to_string(), update_charge_stop) }
                </div>
            </div>
            <div class="row">
                <div class="input_column_halfs">
                    { self.input_block("Effeciency (Miles per KWH)".to_string(), stored_data.vehicle_efficiency.to_string(), update_mpkwh) }
                </div>
                <div class="input_column_halfs">
                    { self.input_block("Charging Rate (KWH)".to_string(), stored_data.charging_rate.to_string(), update_charging_rate) }
                </div>
            </div>
            <div class="row">
                <div class="input_column_halfs">
                    { self.input_block("Reserve Range (Miles)".to_string(), stored_data.reserved_range.to_string(), update_reserve_range) }
                </div>
                <div class="input_column_halfs">
                    { self.input_block("Speed (MPH)".to_string(), stored_data.speed.to_string(), update_speed) }
                </div>
            </div>

            <div class="result-section">
                <div class="result-item">
                    <span class="center">{ "Range" }</span>
                    <div class="center">
                        { self.battery_capacity * self.vehicle_efficiency }
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Usable" }</span>
                    <div class="center">
                        { self.calculate_usable_range().to_string() }
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Charge Time" }</span>
                    <div class="center">
                        { Range::format_as_hours_and_minutes(self.calculate_time_to_charge())}
                    </div>
                </div>
                <div class="result-item">
                    <span class="center">{ "Drive time" }</span>
                    <div class="center">
                        { Range::format_as_hours_and_minutes(self.calculate_drive_time()) }
                    </div>
                </div>
            </div>
            <div class="center-center">
                <button class="grey" onclick={ reset }>{ "reset" }</button>
            </div>
            </>
        }
    }
}

impl Range {

    fn input_block(&self, name: String, data: String, update_fn: yew::Callback<Event>) -> Html {
        html! {
            <>
            <div class={ "input_block" }>
                <label class={ "input_label" }>
                    { name }
                </label>
                <input class={ "input_input" }
                    value={ virtual_dom::AttrValue::from(data)}
                    onchange={ update_fn }
                />
            </div>
            </>
        }
    }

    fn calculate_usable_range(&self) -> f64 {
        let miles_of_range = (self.battery_capacity * self.vehicle_efficiency) as f64;
        let percentage_to_reserve = self.max_charge as f64 / 100_f64;
        let usable_miles_of_range = miles_of_range * percentage_to_reserve - self.reserved_range as f64;

        return usable_miles_of_range;
    }

    fn calculate_time_to_charge(&self) -> f64 {
        if self.charging_rate == 0.0 {
            return 0.0;
        }

        let usable_range = self.calculate_usable_range();

        let time_to_charge = usable_range / self.vehicle_efficiency / self.charging_rate;

        time_to_charge
    }

    fn calculate_drive_time(&self) -> f64 {
        return self.calculate_usable_range() / self.speed;
    }

    fn format_as_hours_and_minutes(hours: f64) -> String {
        let minutes = (hours * 60.0) as i64;

        let buff_hours = if (minutes / 60) < 10 {
            "0"
        } else {
            ""
        };

        let buff_minutes = if (minutes % 60) < 10 { 
            "0" 
        } else { 
            "" 
        };

        return format!("{}{} Hours, {}{} Minutes", buff_hours, minutes / 60, buff_minutes, minutes % 60);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_basic_test_subject() -> Range {
        return Range {
            battery_capacity: 0.0,
            vehicle_efficiency: 0.0,
            reserved_range: 0.0,
            max_charge: 0.0,
            charging_rate: 0.0,
            speed: 0.0,
            pre_built_list: EvList::from(),
        };
    }

    #[test]
    fn usable_range_0() {
        let item_under_test = get_basic_test_subject();

        assert_eq!(0.0, item_under_test.calculate_usable_range());
    }

    #[test]
    fn usable_range_100() {
        let mut item_under_test = get_basic_test_subject();
        item_under_test.battery_capacity =  100.0;
        item_under_test.vehicle_efficiency = 1.0;
        item_under_test.max_charge = 100.0;

        assert_eq!(100.0, item_under_test.calculate_usable_range());
    }

    #[test]
    fn range_400() {
        let item_under_test = Range {
            battery_capacity: 100.0,
            vehicle_efficiency: 4.0,
            reserved_range: 0.0,
            max_charge: 100.0,
            charging_rate: 0.0,
            speed: 0.0,
            pre_built_list: EvList::from(),
        };

        assert_eq!(400.0, item_under_test.calculate_usable_range());
    }

    #[test]
    fn range_350() {
        let item_under_test = Range {
            battery_capacity: 100.0,
            vehicle_efficiency: 4.0,
            reserved_range: 50.0,
            max_charge: 100.0,
            charging_rate: 0.0,
            speed: 0.0,
            pre_built_list: EvList::from(),
        };

        assert_eq!(350.0, item_under_test.calculate_usable_range());
    }

    #[test]
    fn range_300_s() {
        let item_under_test = Range {
            battery_capacity: 100.0,
            vehicle_efficiency: 4.0,
            reserved_range: 0.0,
            max_charge: 75.0,
            charging_rate: 0.0,
            speed: 0.0,
            pre_built_list: EvList::from(),
        };

        assert_eq!(300.0, item_under_test.calculate_usable_range());
    }

    #[test]
    fn range_300_c() {
        let item_under_test = Range {
            battery_capacity: 100.0,
            vehicle_efficiency: 4.0,
            reserved_range: 20.0,
            max_charge: 80.0,
            charging_rate: 0.0,
            speed: 0.0,
            pre_built_list: EvList::from(),
        };

        assert_eq!(300.0, item_under_test.calculate_usable_range());
    }

    #[test]
    fn charge_0() {
        let item_under_test = Range {
            battery_capacity: 100.0,
            vehicle_efficiency: 4.0,
            reserved_range: 0.0,
            max_charge: 0.0,
            charging_rate: 0.0,
            speed: 0.0,
            pre_built_list: EvList::from(),
        };

        assert_eq!(0.0, item_under_test.calculate_time_to_charge());
    }

    #[test]
    fn charge_1() {
        let item_under_test = Range {
            battery_capacity: 100.0,
            vehicle_efficiency: 4.0,
            reserved_range: 0.0,
            max_charge: 100.0,
            charging_rate: 100.0,
            speed: 0.0,
            pre_built_list: EvList::from(),
        };

        assert_eq!(1.0, item_under_test.calculate_time_to_charge());
    }


    #[test]
    fn charge_05() {
        let item_under_test = Range {
            battery_capacity: 100.0,
            vehicle_efficiency: 4.0,
            reserved_range: 20.0,
            max_charge: 80.0,
            charging_rate: 150.0,
            speed: 0.0,
            pre_built_list: EvList::from(),
        };

        assert_eq!(0.5, item_under_test.calculate_time_to_charge());
    }

    #[test]
    fn fmt_0_0() {
        assert_eq!("00 Hours, 00 Minutes", Range::format_as_hours_and_minutes(0.0));
    }

    #[test]
    fn fmt_1_0() {
        assert_eq!("01 Hours, 00 Minutes", Range::format_as_hours_and_minutes(1.0));
    }

    #[test]
    fn fmt_1_5() {
        assert_eq!("01 Hours, 30 Minutes", Range::format_as_hours_and_minutes(1.5));
    }
}

