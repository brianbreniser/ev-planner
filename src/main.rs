// Real stuff
mod base_module;
mod logic;

// Helpers
mod fmt_string;
mod wasm_helper;

// Testing these things
mod just_text;
mod state;
mod test_app;
mod ev_range;

// const DEBUG: bool = false;

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    yew::start_app::<base_module::Base>();
}
