use yew::{html, Component};

use crate::ev_range;

// ==================================== Constants ==
// ==================================== Constants ^^

pub struct Base {}

impl Component for Base {
    type Message = ();
    type Properties = ();

    fn create(_ctx: &yew::Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, _ctx: &yew::Context<Self>) -> yew::Html {
        html! {
            <div class="container">
                <div class="item row_1">
                    <ev_range::Range />
                </div>
            </div>
        }
    }
}
