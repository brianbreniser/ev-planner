use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct VehicleRangeInformation {
    name: String,
    battery_capacity: f64,
    vehicle_efficiency: f64,
    reserved_range: f64,
    max_charge: f64,
    charging_rate: f64,
    speed: f64,
}

pub struct EvList {
}

impl EvList {
    pub fn from() -> Vec<VehicleRangeInformation> {
        vec![
            VehicleRangeInformation {
                name: "Empty".to_string(),
                battery_capacity: 0.0,
                vehicle_efficiency: 0.0,
                reserved_range: 0.0,
                max_charge: 0.0,
                charging_rate: 0.0,
                speed: 0.0,
            }
        ]
    }
}
