#!/bin/env bash
set -e

./podman_login.sh

cd ..

podman build -t evplanner -f Dockerfile

podman tag localhost/evplanner quay.io/breniserbrian/evplanner:latest

podman push quay.io/breniserbrian/evplanner:latest

notify-send "Done building to podman locally and pushing to quay"

