#!/bin/env bash
set -e

./podman_login.sh

podman build -t evplanner -f Dockerfile

podman tag localhost/evplanner quay.io/breniserbrian/evplanner:latest

podman push quay.io/breniserbrian/evplanner:latest

kubectl -n evplanner rollout restart deployment/frontend

notify-send "Done uploading EV Planner to Kubernetes"

