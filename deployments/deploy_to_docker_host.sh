#!/bin/env bash
set -e

echo "======================"
echo "SSH-ing into host"
echo "======================"

echo "SSH to where?"

./run_docker.sh

echo "======================"
echo "Done"
echo "======================"

notify-send "Done deploying to docker host"

exit


